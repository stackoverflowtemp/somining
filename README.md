# README #

## Part Of Speech Tagger for Stack Overflow ##

* Software for (1) detecting code islands and (2) POS tagging of natural language in Stack Overflow posts.
* Latest release: 1.0.0 (3/29/2016)

### Usage ###

* Make sure [Java 8](http://www.oracle.com/technetwork/java/javase/overview/index.html) is installed on your machine.
* Download [resources](https://bitbucket.org/stackoverflowtemp/somining/src/3d5b03faf7eaeb0903a6cc8443438da7769bfa62/resources/?at=master) and [sopostagger.jar](https://bitbucket.org/stackoverflowtemp/somining/src/3d5b03faf7eaeb0903a6cc8443438da7769bfa62/sopostagger.jar?fileviewer=file-view-default) to the same directory.
* Run the following command: `java -jar sopostagger.jar path_to_directory_with_your_data_files`

### Output ###

* Two forms of output will be produced: (1) text files annotated with code islands in `./output/code-island-output` and (2) pos tagged text files with annotated code islands in `./output/pos-tagged-output`