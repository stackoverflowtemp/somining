package codeislandminer.postagging.ui;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;

import codeislandminer.postagging.Tagger;
import codeislandminer.segmentation.Annotator;
import codeislandminer.segmentation.utils.Features;

public class Main {

	public static void main(String[] args) throws IOException {
		if (args.length != 1) {
			System.out.println("Usage: data-directory");
			System.exit(1);
		}

		File dataDir = new File(args[0]);
		if (!dataDir.isDirectory() || !dataDir.exists()) {
			System.out.println("Please enter a valid data directory with text files for processing.");			
			System.exit(1);
		}
		
		Collection<File> files = FileUtils.listFiles(dataDir, new RegexFileFilter(".*$"), DirectoryFileFilter.DIRECTORY);
		if (files.isEmpty()) {
			System.out.println("No files in input data directory: "+args[0]+".");
			System.exit(1);
		}
		
		File outputDir = new File("/output");
		if (!outputDir.exists()) {
			outputDir.mkdirs();
		}
		
		//annotate for code islands
		Features.init();
		Annotator.setModelFile("annotate");
		Annotator annotator = new Annotator(files);
		annotator.annotate();

		//pos tag the code island annotated data
		Tagger tag = new Tagger();
		tag.postag();
	}

}
