package codeislandminer.postagging;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import edu.emory.clir.clearnlp.component.AbstractComponent;
import edu.emory.clir.clearnlp.component.configuration.DecodeConfiguration;
import edu.emory.clir.clearnlp.component.utils.GlobalLexica;
import edu.emory.clir.clearnlp.component.utils.NLPMode;
import edu.emory.clir.clearnlp.component.utils.NLPUtils;
import edu.emory.clir.clearnlp.dependency.DEPNode;
import edu.emory.clir.clearnlp.dependency.DEPTree;
import edu.emory.clir.clearnlp.tokenization.AbstractTokenizer;
import edu.emory.clir.clearnlp.util.IOUtils;

public class Tagger {
		
	final static String POS_TAG_DIR = "output/pos-tagged-output/";
	final static String CONFIG = "resources/config_decode_myposlex.xml";		
	static {
		GlobalLexica.init(IOUtils.createFileInputStream(CONFIG));
	}	
	static DecodeConfiguration config = new DecodeConfiguration(IOUtils.createFileInputStream(CONFIG));
	static AbstractTokenizer tokenizer = NLPUtils.getTokenizer(config.getLanguage());
	static AbstractComponent component = NLPUtils.getPOSTagger(config.getLanguage(), config.getModelPath(NLPMode.pos));
	
	Collection<File> annotationFiles;
		
	public Tagger() throws IOException {
		this.annotationFiles = FileUtils.listFiles(new File("output/code-island-output"), new RegexFileFilter(".*\\.txt$"), DirectoryFileFilter.DIRECTORY);
		if (!new File(POS_TAG_DIR).exists()) {
			new File(POS_TAG_DIR).mkdirs();
		}
	}	
		
	public static String tag(DEPTree tree, Map<String, List<String>> codeDataMap) {
		component.process(tree);
		String sentence = "";		
		DEPNode[] nodes = tree.toNodeArray();
		for (int i = 1; i < nodes.length; i++) {
			
			if (codeDataMap.containsKey(nodes[i].getWordForm())) {
				List<String> codeData = codeDataMap.get(nodes[i].getWordForm());
				String code = codeData.get(0);
				codeData.remove(0);
				sentence += "<"+nodes[i].getWordForm()+">"+code+" ";
				continue;
			}
			
			sentence += nodes[i].getWordForm()+"/"+nodes[i].getPOSTag()+" ";
		}
		sentence = sentence.trim();
		return sentence;
	}		
	
	public static void tag(String text, Map<String, List<String>> codeDataMap, FileOutputStream output) throws IOException {
		List<List<String>> tokens = tokenizer.segmentize(new ByteArrayInputStream(text.getBytes()));
		
		int i, size = tokens.size();
		DEPTree tree;
		String outputString = "";
		
		for (i=0; i<size; i++) {
			tree = new DEPTree(tokens.get(i));
			outputString += tag(tree, codeDataMap)+"\n";
		}
			
		output.write(outputString.getBytes());
	}
			
	public void postag() throws IOException {
		for (File annotationFile : annotationFiles) {
			String[] lines = FileUtils.readFileToString(annotationFile).split("\\n");
			String newOutputString = "";
			String codeString = "";
			String tag = "";	
			Map<String, List<String>> codeDataMap = Maps.newHashMap();
			boolean first = true;
			for (int i = 0; i < lines.length; i++) {
				lines[i] = lines[i].trim();
				String[] tokens = lines[i].split("\\s");
				for (int j = 0; j < tokens.length; j++) {
					tag = tokens[j].contains("<othercode>") ? "othercode" : tokens[j].contains("<javacode>") ? "javacode" : tokens[j].contains("<errorcode>") ? "errorcode" : tag;
					tokens[j] = tokens[j].replace("<"+tag+">", "").trim();
					if (!tag.equals("")) {
						if (first) {
							newOutputString += tag+" ";
							first = false;
						}
						if (tokens[j].contains("</"+tag+">")) {
							String[] subTokens = tokens[j].split("</"+tag+">");
							codeString += subTokens[0]+"</"+tag+">";
							if (j == tokens.length-1 && subTokens.length == 1) {
								codeString += "\n";
							}
							List<String> code = codeDataMap.get(tag);
							if (code == null) {
								codeDataMap.put(tag, code = Lists.newLinkedList());
							}
							code.add(codeString);
							codeString = "";
							tag = "";
							first = true;							
							for (int k = 1; k < subTokens.length; k++) {
								newOutputString += subTokens[k]+" ";
							}
							continue;
						}
						if (!tokens[j].equals("")) {
							codeString += tokens[j]+" ";
						}
					}
					else {
						newOutputString += tokens[j]+" ";
					}
				}
				if (!tag.equals("")) {
					if (!codeString.equals("")) {
						codeString += "\n";
					}
				}
				else {
					newOutputString += "\n";
				}
			}
			tag(newOutputString, codeDataMap, new FileOutputStream(POS_TAG_DIR+annotationFile.getName()));
		}
	}
	
	public static void main(String[] args) throws Exception {		
		String fileString = FileUtils.readFileToString(new File("src/test/resources/check.txt"));
		tag(fileString, Maps.newHashMap(), new FileOutputStream("src/test/resources/check-out.txt"));		
	}	
}
