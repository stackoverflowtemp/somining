package codeislandminer.segmentation.data;

public class Token {
	
	public String word;
	public int index;
	public String classVal;	
	public int isFirstWord;

	/*public String printIfSpecialChar(String word) {
		if (word.length() > 1) {
			return word;
		}
		int ascii = word.charAt(0);
		if (ascii == 7) {
			System.out.println(word);
			System.exit(1);
		}
		return word;
	}*/
	
	public Token(String word, int index, boolean isFirstWord) {
		//this.word = printIfSpecialChar(word);
		this.word = word;
		this.index = index;
		this.isFirstWord = isFirstWord ? 1 : 0;
	}
	
	public void setClassVal(String classVal) {
		this.classVal = classVal;
	}
	
}
