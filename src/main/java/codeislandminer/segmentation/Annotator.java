package codeislandminer.segmentation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;

import codeislandminer.segmentation.data.Token;
import codeislandminer.segmentation.utils.Features;
import opennlp.tools.ml.model.GenericModelReader;
import opennlp.tools.ml.model.MaxentModel;
import opennlp.tools.ml.model.RealValueFileEventStream;

public class Annotator {
	
	Collection<File> annotationFiles;
	static String modelFile;
	MaxentModel model;	
	Tokenizer tokenizer;		
	
	public static void setModelFile(String annotateType) {
		modelFile = annotateType.equals("annotate") ? "resources/maxentSOModel.txt" : "resources/maxentModel.txt";
	}
	
	public Annotator(Collection<File> annotationFiles) throws IOException {
		this.annotationFiles = annotationFiles;
		this.model = new GenericModelReader(new File(modelFile)).getModel();
	}	

	private String getCodeTag(String classVal) {
		return classVal.equals("code") ? "othercode" : classVal.equals("error") ? "errorcode" : classVal.equals("javacode") ? classVal : "";
	}
	
	private void writeOutput(List<Token> tokens, FileOutputStream output) throws IOException {
		Token token = tokens.get(0);
		String prevCodeTag = getCodeTag(token.classVal);
		String outputSentence = "";
		String outputStr = "";
		if (!prevCodeTag.equals("")) {
			outputSentence = "<"+prevCodeTag+">";
		}
		outputSentence += token.word;
		for (int i = 1; i < tokens.size(); i++) {
			token = tokens.get(i);
			String codeTag = getCodeTag(token.classVal);
			
			if ((Features.keywords.contains(token.word) || token.word.equals(";") || token.word.equals("{") || token.word.equals("}") || token.word.equals("(") || token.word.equals(")")) && prevCodeTag.equals("javacode")) {
				codeTag = prevCodeTag;
			}
			
			if (!codeTag.equals("javacode") && prevCodeTag.equals("javacode") && (i+1 >= tokens.size() || tokens.get(i+1).classVal.equals("javacode"))) {
				codeTag = prevCodeTag;
			}
			
			if (!prevCodeTag.equals("") && !codeTag.equals(prevCodeTag)) {
				outputSentence += "</"+prevCodeTag+">";
			}
			
			if (token.isFirstWord == 1) {
				outputSentence = outputSentence.trim(); 
				outputStr += outputSentence+"\n";
				outputSentence = "";
			}			
			
			if (!codeTag.equals("") && !codeTag.equals(prevCodeTag)) {
				outputSentence += " <"+codeTag+">"+token.word;
			}
			else {
				outputSentence += token.index == 0 ? " "+token.word : token.word;
			}
			prevCodeTag = codeTag;
		}
		
		if (!prevCodeTag.equals("") && !outputSentence.equals("") && !outputSentence.matches(".*?</"+prevCodeTag+">")) {
			outputSentence += "</"+prevCodeTag+">";
		}
		
		if (!outputSentence.equals("")) {
			outputSentence = outputSentence.trim();
			outputStr += outputSentence+"\n";
			outputSentence = "";
		}
		
		output.write(outputStr.getBytes());
	}
	
	private String predict (String[] contexts) {
		float[] values = RealValueFileEventStream.parseContexts(contexts);
		return model.getBestOutcome(model.eval(contexts,values));
	}		
	
	private String featureStr(Token token, List<Token> tokens, int i) {
		Features feat = new Features(token);
		feat.setUniFV();
		feat.setContextFW(token, tokens, i);
		return Features.clean(feat.toString());
	}	
	
	private void predict(String fileString, FileOutputStream output) throws IOException {
		Tokenizer tokenize = new Tokenizer();
		List<Token> tokens = tokenize.execute(fileString);
		
		for (int i = 0; i < tokens.size(); i++) {
			Token token = tokens.get(i);
			token.classVal = predict(featureStr(token, tokens, i).split(" "));
		}
		
		writeOutput(tokens, output);
	}
		
	public void annotate() throws IOException {
		String outputDir = "output/code-island-output";
		if (!new File(outputDir).exists()) {
			new File(outputDir).mkdirs();
		}
		for (File annotationFile : annotationFiles) {
			String annotationFileContent = FileUtils.readFileToString(annotationFile);
			predict(annotationFileContent, new FileOutputStream(outputDir+"/"+annotationFile.getName()));			
		}
	}	
	
}
