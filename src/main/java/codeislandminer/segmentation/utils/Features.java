package codeislandminer.segmentation.utils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Lists;

import codeislandminer.segmentation.data.Token;

public class Features {

	public static final List<String> OPERATORS = Lists.newArrayList("+", "-", "/", "%", "*", "=", "<", ">", "&", "!");
	public static List<String> emo2 = Lists.newArrayList();
	public static List<String> emo3 = Lists.newArrayList();
	public static List<String> keywords = Lists.newArrayList();
		
	public static void setKeywords() throws IOException {
		String[] lines = FileUtils.readFileToString(new File("resources/keywords.txt")).split("\\n");
		for (String line : lines) {
			line = line.trim();
			keywords.add(line);
		}
	}

	public static void setEmoticons() throws IOException {
		String[] lines = FileUtils.readFileToString(new File("resources/emoticons.txt")).split("\\n"); 
		for (String line : lines) {
			line = line.trim();
			if (line.length() == 2) {
				emo2.add(line);
			}
			else if (line.length() == 3) {
				emo3.add(line);
			}
		}
	}
	
	public static void init() throws IOException {
		setKeywords();
		setEmoticons();
	}
		
    public static final String NV = "__nil__";    
    
    public String word;
    public int sizeInChars;
    public int isFirstWord;
    public String prefix1;
    public String prefix2;
    public String prefix3; 
    public String suffix1;
    public String suffix2;
    public String suffix3;
    public String wordShape;
    public String wordShapeShort;
    public int allCaps;
    public int initCaps;
    public int typeCase;
    public int camel1;
    public int camel2;
    public int hasDigit;
    public int alphaNumeric1;
    public int alphaNumeric2;    
    public int operator;   
    public int keyword;
    
    public String fw;
    public String wFw;
    public String wFwFfw;
    public String pw;
    public String pwW;
    public String ppwPwW;
    public String pwWFw;
    
    public int wFwIsEMO;
    public int pwWIsEMO;
    public int pwWFwIsEMO;
    public int wFwFfwIsEMO;
    public int ppwPwWIsEMO;    
    
    public int commaSepFollowNum;
    public int commaSepPrecedeNum;
    public int spaceSepFollow;
    public int spaceSepPrecede;
    
    public int initOpenSquareBrace;
    public int endCloseSquareBrace;
    public int initOpenParen;
    public int endCloseParen;
    public int initOpenCurly;
    public int endCloseCurly;
    public int initOpenAngle;
    public int endCloseAngle;
    public int initQuotes;
    public int endQuotes;
    public int realNumber;
    
    public String ngramWordShape;
    public String ngramWordShapeShort;     
        
    public Features (Token token) {
        this.word = token.word;
        this.sizeInChars = word.length();
        this.isFirstWord = token.isFirstWord;
    }      
    
    public Features() {
		// TODO Auto-generated constructor stub
	}

	public void setUniFV() {
        prefix1 = setPrefix(word, 1);
        prefix2 = setPrefix(word, 2);
        prefix3 = setPrefix(word, 3);
        suffix1 = setSuffix(word, 1);
        suffix2 = setSuffix(word, 2);
        suffix3 = setSuffix(word, 3); 
        wordShape = setWordShape(word);
        wordShapeShort = setWordShapeShort(word);   
        allCaps = isAllCaps(word);
        initCaps = isInitCaps(word);
        typeCase = isTypeCase(word);
        camel1 = isCamel1(word);
        camel2 = isCamel2(word);
        hasDigit = hasDigit(word);        
        alphaNumeric1 = setAlphaNumeric1(word);
        alphaNumeric2 = setAlphaNumeric2(word);      
        operator = isOperator(word);
        keyword = isKeyword(word);
    }    
    
    public void setContextFW(Token token, List<Token> tokens, int i) {    	
    	Token wTok = i+1 >= tokens.size() ? null : tokens.get(i+1);
    	this.fw = wTok == null ? NV : wTok.isFirstWord == 1 ? "NEWLN" : wTok.word;
    	this.wFw = (this.fw.equals(NV) || this.fw.equals("NEWLN")) ? word+"-"+this.fw : wTok.index == 0 ? word+" "+fw : word+fw;
    	
    	wTok = (i+2 >= tokens.size() || this.fw.equals("NEWLN")) ? null : tokens.get(i+2);
    	this.wFwFfw = wTok == null ? (this.fw.equals("NEWLN") ? this.wFw : this.wFw+"-"+NV) : wTok.isFirstWord == 1 ? this.wFw+"-NEWLN" : wTok.index == 0 ? this.wFw+" "+wTok.word : this.wFw+wTok.word;
    	
    	wTok = (i-1 <= 0 || token.isFirstWord == 1) ? null : tokens.get(i-1);
    	this.pw = token.isFirstWord == 1 ? "NEWLN" : wTok == null ? NV : wTok.word;
    	this.pwW = (this.pw.equals(NV) || this.pw.equals("NEWLN")) ? this.pw+"-"+word : token.index == 0 ? pw+" "+word : pw+word;
    	
    	wTok = (i-2 <= 0 || this.pw.equals("NEWLN")) ? null : tokens.get(i-2);
    	this.ppwPwW = wTok == null ? (this.pw.equals("NEWLN") ? this.pwW : NV+"-"+this.pwW) : wTok.isFirstWord == 1 ? "NEWLN-"+this.pwW : tokens.get(i-1).index == 0 ? wTok.word+" "+this.pwW : wTok.word+this.pwW;

    	this.pwWFw = this.pwW.replace(word, "")+this.wFw;
    	
        wFwIsEMO = setEmoticon(this.wFw, true);
        pwWIsEMO = setEmoticon(this.pwW, true);
        pwWFwIsEMO = setEmoticon(this.pwWFw, false);
        wFwFfwIsEMO = setEmoticon(this.wFwFfw, false);
        ppwPwWIsEMO = setEmoticon(this.ppwPwW, false); 
        commaSepFollowNum = setCommaSepNumber(this.wFw);
        commaSepPrecedeNum = setCommaSepNumber(this.pwW);
        
    	setNgramFV(this.pwWFw);
    }
    
    public void setNgramFV(String ngram) {
    	initOpenSquareBrace = setInitOpenSquareBrace(ngram);
    	endCloseSquareBrace = setEndCloseSquareBrace(ngram);
    	initOpenParen = setInitOpenParen(ngram);
    	endCloseParen = setEndCloseParen(ngram);
    	initOpenCurly = setInitOpenCurly(ngram);
    	endCloseCurly = setEndCloseCurly(ngram);
    	initOpenAngle = setInitOpenAngle(ngram);
    	endCloseAngle = setEndCloseAngle(ngram);
    	initQuotes = setInitQuotes(ngram);
    	endQuotes = setEndQuotes(ngram);
    	realNumber = setRealNumber(ngram);
    	ngramWordShape = setWordShape(ngram);
    	ngramWordShapeShort = setWordShapeShort(ngram);
    }
    
    public int setEmoticon(String ngram, boolean bigram) {
    	return bigram ? ((emo2.contains(ngram) || emo2.contains(StringUtils.reverse(ngram))) ? 1 : 0) : 
    		((emo3.contains(ngram) || emo3.contains(StringUtils.reverse(ngram))) ? 1 : 0); 
    }
    
    public static String setPrefix(String str, int len) {
        if (str.length() < len) {
            return NV;
        }
        return str.substring(0, len);
    }    
    
    public static String setSuffix(String str, int len) {
        if (str.length() < len) {
            return NV;
        }
        return str.substring(str.length() - len);
    }      
    
    public static String setWordShape (String wc) {
        wc = wc.replaceAll("[A-Z]", "A");
        wc = wc.replaceAll("[a-z]", "a");
        wc = wc.replaceAll("[0-9]", "0");
        wc = wc.replaceAll("[^A-Za-z0-9]", "x");
        return wc;
    }

    public static String setWordShapeShort (String bwc) {
        bwc = bwc.replaceAll("[A-Z]+", "A");
        bwc = bwc.replaceAll("[a-z]+", "a");
        bwc = bwc.replaceAll("[0-9]+", "0");
        bwc = bwc.replaceAll("[^A-Za-z0-9]+", "x");
        return bwc;
    }       
    
    public static int isAllCaps(String str) {
        if (str.matches("[A-Z]+")) return 1;
        return 0;
    }    
    
    public static int isInitCaps(String str) {
        if (str.matches("[A-Z][a-z].*")) return 1;
        return 0;
    }    
    
    public static int isTypeCase(String str) {
    	if (str.matches("[A-Z][a-z]+([A-Z][a-z]+)+")) return 1;
    	return 0;
    }

    public static int isCamel1(String str) {
    	if (str.matches("[a-z]+[A-Z][a-z]+")) return 1;
    	return 0;
    }
    
    public static int isCamel2(String str) {
    	if (str.matches("[a-z]+[A-Z][a-z]+([A-Z][a-z]+)+")) return 1;
    	return 0;
    }
    
    public static int hasDigit(String str) {
        if (str.matches(".*[0-9].*")) return 1;
        return 0;
    }

    public static int setAlphaNumeric1(String str) {
        if (str.matches(".*[A-Za-z].*[0-9].*")) return 1;
        return 0;
    }

    public static int setAlphaNumeric2(String str) {
        if (str.matches(".*[0-9].*[A-Za-z].*")) return 1;
        return 0;
    }     

    public static int isOperator(String str) {
    	if (OPERATORS.contains(str)) return 1;
    	return 0;
    }
    
    public static int isKeyword(String str) {
    	if (keywords.contains(str)) return 1;
    	return 0;
    }
    
    public static int setInitOpenSquareBrace(String str) {
        if (str.matches("\\[.*")) return 1;
        return 0;
    }
    
    public static int setEndCloseSquareBrace(String str) {
        if (str.matches(".*\\]")) return 1;
        return 0;
    }    
    
    public static int setInitOpenParen(String str) {
    	if (str.matches("\\(.*")) return 1;
    	return 0;
    }
    
    public static int setEndCloseParen(String str) {
    	if (str.matches(".*\\)")) return 1;
    	return 0;    	
    }
    
    public static int setInitOpenCurly(String str) {
    	if (str.matches("\\{.*")) return 1;
    	return 0;
    }
    
    public static int setEndCloseCurly(String str) {
    	if (str.matches(".*\\}")) return 1;
    	return 0;    	
    }
    
    public static int setInitOpenAngle(String str) {
    	if (str.matches("<.*")) return 1;
    	return 0;
    }
    
    public static int setEndCloseAngle(String str) {
    	if (str.matches(".*>")) return 1;
    	return 0;
    }
    
    public static int setInitQuotes(String str) {
    	if (str.matches("\".*")) return 1;
    	return 0;
    }
    
    public static int setEndQuotes(String str) {
    	if (str.matches(".*\"")) return 1;
    	return 0;
    }
    
    public static int setRealNumber(String str) {
        if (str.matches("[-0-9]+[.,]+[0-9.,]+")) return 1;
        return 0;
    }
    
    public static int setCommaSepNumber(String str) {
    	if (str.matches("[0-9]+[,][0-9]+")) return 1;
    	return 0;
    }
    
    @Override
    public String toString() {
        return
                "w:"+word + " " + 
                "size:"+sizeInChars + " " +
                "is1stWord:"+isFirstWord + " " +
                "pref1:"+prefix1 + " " +
                "pref2:"+prefix2 + " " +
                "pref3:"+prefix3 + " " +
                "suf1:"+suffix1 + " " +
                "suf2:"+suffix2 + " " +
                "suf3:"+suffix3 + " " +
                "shape:"+wordShape + " " + 
                "shapeShort:"+wordShapeShort + " " +
                "allCaps:"+allCaps + " " +
                "initCaps:"+initCaps + " " + 
                "typeCase:"+typeCase + " " +
                "camel1:"+camel1 + " " +
                "camel2:"+camel2 + " " +
                "hasDigit:"+hasDigit + " " + 
                "alphaN1:"+alphaNumeric1 + " " +
                "alphaN2:"+alphaNumeric2 + " " +
                "operator:"+operator + " " +
                "keyword:"+keyword + " " +                
                "fw:"+fw+" "+
                "wfw:"+wFw+" "+
                "wfwfw:"+wFwFfw+" "+
                "pww:"+pwW+" "+
                "pwpww:"+ppwPwW+" "+
                "pwwfw:"+pwWFw+" "+
                "wfwIsEMO:"+wFwIsEMO+" "+
                "pww:"+pwWIsEMO+" "+
                "pwwfw:"+pwWFwIsEMO+" "+
                "wfwfw:"+wFwFfwIsEMO+" "+
                "pwpwwIsEMO:"+ppwPwWIsEMO+" "+
                "wfwCommaSep:"+commaSepFollowNum+" "+
                "pwwCommaSep:"+commaSepPrecedeNum+" "+
                "initOpenSquareBrace:"+initOpenSquareBrace+" "+
                "endCloseSquareBrace:"+endCloseSquareBrace+" "+
                "initOpenParen:"+initOpenParen+" "+
                "endCloseParen:"+endCloseParen+" "+
                "initOpenCurly:"+initOpenCurly+" "+
                "endCloseCurly:"+endCloseCurly+" "+
                "initOpenAngle:"+initOpenAngle+" "+
                "endCloseAngle:"+endCloseAngle+" "+
                "initQuotes:"+initQuotes+" "+
                "endQuotes:"+endQuotes+" "+
                "realNumber:"+realNumber+" "+
                "ngramWordShape:"+ngramWordShape+" "+
                "ngramWordShapeShort:"+ngramWordShapeShort;
    }
    
    public static String clean(String string) {
    	return string.replaceAll("null", NV).replaceAll("\"", "q").replaceAll("=", "eq");
    }	
	
}
