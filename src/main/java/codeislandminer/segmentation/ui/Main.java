package codeislandminer.segmentation.ui;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;

import codeislandminer.segmentation.Annotator;
import codeislandminer.segmentation.Trainer;
import codeislandminer.segmentation.utils.Features;

public class Main {
	
	public static void main(String[] args) throws IOException {
		if (args.length != 2) {
			System.out.println("Usage: data-directory [train|annotate|trainedAnnotate]");
			System.exit(1);
		}
		
		if (!args[1].equals("train") && !args[1].equals("annotate")) {
			System.out.println("Please enter one of the available options\n:train\nannotate");
			System.exit(1);
		}

		File dataDir = new File(args[0]);
		if (!dataDir.isDirectory() || !dataDir.exists()) {
			System.out.println("Please enter a valid data directory with SO text files for processing.");
			System.exit(1);
		}
		
		Collection<File> files = FileUtils.listFiles(dataDir, new RegexFileFilter(".*\\.txt$"), DirectoryFileFilter.DIRECTORY);
		if (files.isEmpty()) {
			System.out.println("No files in input data directory: "+args[0]+".");
			System.exit(1);
		}
				
		Features.init();
		if (args[1].equals("train")) {
			//train the code island annotator
			Trainer train = new Trainer(files);
			train.train();
		}
		else if (args[1].contains("annotate")) {
			Annotator.setModelFile(args[1]);
			Annotator annotator = new Annotator(files);
			annotator.annotate();
		}
		
	}

}
