package codeislandminer.segmentation;

import java.io.IOException;
import java.util.List;

import com.google.common.collect.Lists;

import codeislandminer.segmentation.data.Token;

public class Tokenizer {
	
	String text;
	
    public Tokenizer(String input) {
		this.text = input;
	}
    
    public Tokenizer() {
    	this.text = null;
    }
    
    public List<Token> addToken(String word, int index, boolean isFirstWord, String classVal, List<Token> tokens) {
    	Token token = new Token(word, index, isFirstWord);
    	token.setClassVal(classVal);
    	tokens.add(token);
    	return tokens;
    }
    
	public List<Token> tokenize(char[] charArr, boolean isFirstWord, String classVal, List<Token> tokens) throws IOException {
		String word = "";
		int index = 0;
		for (int i = 0; i < charArr.length; i++) {
			String cStr = Character.toString(charArr[i]);	
			if (cStr.matches("[a-zA-Z0-9]")) {
				word += cStr;
			}
			else {
				if (!word.equals("")) {
					tokens = addToken(word, index-word.length(), isFirstWord, classVal, tokens);
					isFirstWord = false;
					word = "";
				}
				tokens = addToken(cStr, index, isFirstWord, classVal, tokens);
				isFirstWord = false;
			}
			index++;					
		}
		if (!word.equals("")) {
			index--;
			tokens = addToken(word, index, isFirstWord, classVal, tokens);
		}
		return tokens;
	}    
    
	public List<Token> tokenize(String word, boolean isFirstWord, String classVal, List<Token> tokens) throws IOException {
		if (word.matches("[a-zA-Z0-9]+") || word.matches("_+") || word.matches("\\d+")) {
			Token token = new Token(word, 0, isFirstWord);
			token.setClassVal(classVal);
			tokens.add(token);
		}
		else {
			tokens = tokenize(word.toCharArray(), isFirstWord, classVal, tokens);
		}
		return tokens;
	}    
    
	/**
	 * Function that tokenizes text for code island detection.
	 */
	public List<Token> execute() throws IOException {
		List<Token> tokens = Lists.newLinkedList();
		
		String classVal = "word";		
		String[] lines = text.split("\\n");
		for (int k = 0; k < lines.length; k++) {
			String line = lines[k].trim();
			
			if (line.equals("")) {
				continue;
			}
			
			if (line.equals("<code>") || line.equals("<javacode>") || line.equals("<error>") ||
					line.equals("</code>") || line.equals("</javacode>") || line.equals("</error>")) {
				classVal = line.matches("</.*") ? "word" : line.replace("<", "").replace(">", "");				
				continue;
			}
			String[] lineTokens = line.split("\\s+");
			boolean isFirstWord = true;
			for (int i = 0; i < lineTokens.length; i++) {
				if (!classVal.equals("word") && lineTokens[i].equals("</"+classVal+">")) {
					classVal = "word";
					continue;
				}
				
				if (classVal.equals("word") && lineTokens[i].matches(".*?<code>.*")) {
					String word = lineTokens[i].substring(0, lineTokens[i].indexOf("<code>"));
					tokens = tokenize(word, isFirstWord, classVal, tokens);		
					isFirstWord = false;
					lineTokens[i] = lineTokens[i].replace(word+"<code>", "");
					classVal = "code";
				}
				else if (classVal.equals("word") && lineTokens[i].matches(".*?<javacode>.*")) {
					String word = lineTokens[i].substring(0, lineTokens[i].indexOf("<javacode>"));
					tokens = tokenize(word, isFirstWord, classVal, tokens);
					isFirstWord = false;
					lineTokens[i] = lineTokens[i].replace(word+"<javacode>", "");
					classVal = "javacode";
				}
				else if (classVal.equals("word") && lineTokens[i].matches(".*?<error>.*")) {
					String word = lineTokens[i].substring(0, lineTokens[i].indexOf("<error>"));
					tokens = tokenize(word, isFirstWord, classVal, tokens);
					isFirstWord = false;
					lineTokens[i] = lineTokens[i].replace(word+"<error>", "");
					classVal = "error";
				}								
				else if (lineTokens[i].matches("<code>.*")) {
					lineTokens[i] = lineTokens[i].replace("<code>", "");
					classVal = "code";
				}
				else if (lineTokens[i].matches("<javacode>.*")) {
					lineTokens[i] = lineTokens[i].replace("<javacode>", "");
					classVal = "javacode";
				}
				else if (lineTokens[i].matches("<error>.*")) {
					lineTokens[i] = lineTokens[i].replace("<error>", "");
					classVal = "error";
				}
				if (lineTokens[i].matches(".*?</"+classVal+">.*")) {
					String[] subTokens = lineTokens[i].split("</"+classVal+">");
					tokens = tokenize(subTokens[0], isFirstWord, classVal, tokens);
					isFirstWord = false;
					classVal = "word";
					for (int j = 1; j < subTokens.length; j++) {
						tokens = tokenize(subTokens[j], isFirstWord, classVal, tokens);
					}
					continue;
				}
				tokens = tokenize(lineTokens[i], isFirstWord, classVal, tokens);
				isFirstWord = false;
			}
			
		}
		return tokens;
	}	

	/**
	 * Function that tokenizes text for code island detection.
	 */
	public List<Token> execute(String text) throws IOException {
		List<Token> tokens = Lists.newLinkedList();
		
		String[] lines = text.split("\\n");
		for (int k = 0; k < lines.length; k++) {
			String line = lines[k].trim();
			
			if (line.equals("")) {
				continue;
			}		
		
			String[] lineTokens = line.split("\\s+");
			boolean isFirstWord = true;
			for (int i = 0; i < lineTokens.length; i++) {
				tokens = tokenize(lineTokens[i], isFirstWord, "", tokens);
				isFirstWord = false;
			}
		}
		return tokens;
	}		
	
}
