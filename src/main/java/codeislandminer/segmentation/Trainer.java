package codeislandminer.segmentation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;

import codeislandminer.segmentation.data.Token;
import codeislandminer.segmentation.utils.Features;
import opennlp.tools.ml.maxent.GIS;
import opennlp.tools.ml.maxent.RealBasicEventStream;
import opennlp.tools.ml.maxent.io.GISModelWriter;
import opennlp.tools.ml.maxent.io.SuffixSensitiveGISModelWriter;
import opennlp.tools.ml.model.AbstractModel;
import opennlp.tools.ml.model.OnePassRealValueDataIndexer;
import opennlp.tools.util.PlainTextByLineStream;

public class Trainer {

	static boolean USE_SMOOTHING = false;
	static double SMOOTHING_OBSERVATION = 0.1;
	
	Collection<File> trainingFiles;
		
	public Trainer(Collection<File> trainingFiles) throws IOException {
		this.trainingFiles = trainingFiles;
	}
	
	Tokenizer tokenizer;	
	Features feat;
	
	public void createModel(String trainFile, String modelFile) {
		try {
			FileReader datafr = new FileReader(new File(trainFile));
			RealBasicEventStream es = new RealBasicEventStream(new PlainTextByLineStream(datafr));
			GIS.SMOOTHING_OBSERVATION = SMOOTHING_OBSERVATION;
			AbstractModel model = GIS.trainModel(100, new OnePassRealValueDataIndexer(es,0), USE_SMOOTHING);			
			File outputFile = new File(modelFile);
			GISModelWriter writer =  new SuffixSensitiveGISModelWriter(model, outputFile);
			writer.persist();
		} catch (Exception e) {
			System.out.print("Unable to create model due to exception: ");
			System.out.println(e);
			e.printStackTrace();
		}		
	}
	
	public String featureStr(Token token, List<Token> tokens, int i) {
		Features feat = new Features(token);
		feat.setUniFV();
		feat.setContextFW(token, tokens, i);
		return Features.clean(feat.toString()+" "+token.classVal);
	}
	
	public void writeTrainingData(FileOutputStream output) throws IOException {
		for (File trainingFile : trainingFiles) {
			
			String text = FileUtils.readFileToString(trainingFile);
			//byte[] array = text.getBytes("UTF-8");
			
			tokenizer = new Tokenizer(text); 
			List<Token> tokens = tokenizer.execute();			
			int totalTokens = tokens.size();
			
			String featureStr = "";
			for (int i = 0; i < totalTokens; i++) {
				Token token = tokens.get(i);
				featureStr += featureStr(token, tokens, i)+"\n";
			}
			output.write(featureStr.getBytes());
		}		
	}
		
	public void train() throws IOException {
		String trainFile = "resources/maxentTrainingData.txt";
		writeTrainingData(new FileOutputStream(trainFile));
		String modelFile = "resources/maxentModel.txt";
		createModel(trainFile, modelFile);
	}
	
}
